package com.example.sessionmanagement;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    //this is the first page that appears to the user
    EditText uName, pwd;
    Button btnLogin, btnRegister;
    SharedPreferences pref;
    Intent intent;
    Intent intentR;
    FileInputStream fStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        uName = findViewById(R.id.edtUserName);
        pwd = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);

        //setting a name to the shared preferences storage
        pref = getSharedPreferences("loginInformation", MODE_PRIVATE);

        //setting possible intents (other activities/pages that the app can show)
        intent = new Intent(MainActivity.this, LoggedActivity.class);
        intentR = new Intent(MainActivity.this, RegisterActivity.class);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = uName.getText().toString();
                String password = pwd.getText().toString();
                try {
                    //internal storage "registerDetails" contains the registered users to check in the login
                    fStream = openFileInput("registerDetails");
                    StringBuffer sBuffer = new StringBuffer();
                    int i;

                    //receiving data from internal storage until the "end"
                    while((i = fStream.read()) != -1){
                        sBuffer.append((char)i);
                    }
                    fStream.close();

                    //insert each data (username and password) and a position of the array
                    String registerDetails[] = sBuffer.toString().split("\n");

                    if((registerDetails[0].equals(username)) && (registerDetails[1].equals(password))){
                        //if the user inserted correct name and password, save on shared preferences his username
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("userName", registerDetails[0]);
                        editor.commit();
                        //starting the activity LoggedActivity.class
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid credentials", Toast.LENGTH_LONG).show();
                    }

                } catch (FileNotFoundException e){
                    Toast.makeText(getApplicationContext(), "Please register first", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if the user clicks on register button then start the activity RegisterActivity.class
                startActivity(intentR);
            }
        });
    }
}
